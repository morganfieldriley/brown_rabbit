function addImage(parent, src){
  let image = document.createElement("img");
  image.src = src;
  parent.appendChild(image);
}

function addSponsors(){
  let sponsors_box = document.getElementById("sponsors");
  for (var i = 1; i <= 20; i++) {
    addImage(sponsors_box, `assets/images/sponsors/sponsor-${i}.png`)
  }
}

function addInstaImg(){
  let insta_box = document.getElementById("insta_images");
  for (var i = 1; i <= 9; i++) {
    addImage(insta_box, `assets/images/instagram-feed/instagram-image-${i}.png`)
  }
}

addSponsors();
addInstaImg();
