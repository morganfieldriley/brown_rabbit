function findTitles(){

  let coffee_reads = document.getElementsByClassName('card-body')
  let coffee_reads_titles = []

  for (let i = 0; i < coffee_reads.length; i++) {
    coffee_reads_titles.push(coffee_reads[i].getElementsByClassName('card-title')[0].innerHTML.toLowerCase())
  }
  return coffee_reads_titles;
}

function findMatches(input, titles){

  input = input.toLowerCase();
  let matches = [];
  let string = "hello";

  for(let title of titles){
    if(title.includes(input)){
      matches.push(title);
    }
  }
  return matches;

}

let titles = findTitles();

let input = document.querySelector('input');
let search_results = document.getElementById('search_results');
console.log(search_results);
let body = document.querySelector('body');

input.addEventListener('input', handleChange);

function handleChange(event) {
  findMatches(event.target.value, titles);
}


