let current_page = 1;
let cards_per_page = 5;


window.onload = () => {

  let articles = createArticleList();

  addArticlesToCards(cards_per_page, articles, current_page);

  createModalClickEvents(cards_per_page, articles);

  setUpSearchBar(articles);

  setupPagination(cards_per_page, articles);
}


class Article{
  constructor(title, image_url){
    this.title = title;
    this.image_url = image_url;
    this.body = String.raw`
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed volutpat, felis quis ultrices porttitor,
      lorem sem pharetra purus, sed tincidunt neque ligula ut augue. Phasellus cursus ac mauris sit amet volutpat.
      In efficitur magna id risus malesuada, sit amet efficitur felis maximus. Ut sit amet rutrum nisl, non tempus purus.
      llam neque nunc, tempor ac dui non, blandit tristique dui. Maecenas tincidunt ligula tortor, ac auctor augue posuere vel.
      Integer et ex varius, pellentesque libero quis, eleifend ligula. Nunc ut ex tortor.

      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed volutpat, felis quis ultrices porttitor,
      lorem sem pharetra purus, sed tincidunt neque ligula ut augue. Phasellus cursus ac mauris sit amet volutpat.
      In efficitur magna id risus malesuada, sit amet efficitur felis maximus. Ut sit amet rutrum nisl, non tempus purus.
      llam neque nunc, tempor ac dui non, blandit tristique dui. Maecenas tincidunt ligula tortor, ac auctor augue posuere vel.
      Integer et ex varius, pellentesque libero quis, eleifend ligula. Nunc ut ex tortor.

      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed volutpat, felis quis ultrices porttitor,
      lorem sem pharetra purus, sed tincidunt neque ligula ut augue. Phasellus cursus ac mauris sit amet volutpat.
      In efficitur magna id risus malesuada, sit amet efficitur felis maximus. Ut sit amet rutrum nisl, non tempus purus.
      llam neque nunc, tempor ac dui non, blandit tristique dui. Maecenas tincidunt ligula tortor, ac auctor augue posuere vel.
      Integer et ex varius, pellentesque libero quis, eleifend ligula. Nunc ut ex tortor.`
  }

  preview() {
    return this.body.substring(0, 200);
  }

  dropdown() {
    return this.body.substring(0, 100);
  }
}



function createArticleList(){

  let titles = [
   "Espresso",
   "Coffee",
   "Mocha",
   "Latte",
   "Cortado",
   "Cafe con Leche",
   "Italiano",
   "Americano",
   "Cold Brew",
   "Iced Coffee",
   "Doppio",
   "Pumpkin Spice is Disgusting",
   "Starbucks Causes Brain Cancer",
   "Nitro Brew",
   "Arabica: a Modern God",
   "Where is Sumatra?",
  ];

  let articles = []

  let image_number = 1;

  for(let title of titles){
    let image_url = `assets/images/posts/Post thumbnail-${image_number}.png`
    articles.push(new Article(title, image_url));
    image_number += 1
  }
  return articles;
}


function addArticlesToCards(cards_per_page, articles, page_number){

  console.log(page_number);
  let offset = (page_number - 1) * cards_per_page;

  for(let index = 0; index < cards_per_page; index++){
    let article = articles[index + offset];

    let title_box =  document.getElementById(`title_${index}`);
    let body_box = document.getElementById(`body_${index}`);
    let image_box = document.getElementById(`image_${index}`);

    title_box.textContent = article.title;
    body_box.textContent = article.preview();
    image_box.src = article.image_url;
  }
}


function createModalClickEvents(cards_per_page, articles){

  let modal_title = document.getElementById("modal-title")
  let modal_body = document.getElementById("modal-body")
  let modal_img = document.getElementById("modal-img")

  for(let index = 0; index < cards_per_page; index++){

    let article = articles[index];

    let button_box = document.getElementById(`read_more_${index}`);

    button_box.addEventListener('click', function(event){

      modal_title.textContent = article.title;
      modal_body.textContent = article.body;
      modal_img.src = article.image_url;
    });
  }
}

function filterArticles(articles, query){

    let search_results = []

    for(let article of articles){

      if(article.title.toLowerCase().includes(query.toLowerCase())){
        search_results.push(article)
      }
    }
    return search_results
}


function addElement(parent, tag){
  let element = document.createElement(tag);
  parent.appendChild(element);
}

function createDropdownItem(article){
  let dropdown_item = document.createElement("div")
  dropdown_item.id = "dropdown_item";

  let title = document.createElement("h5");
  let body = document.createElement("p");

  title.textContent = article.title;
  body.textContent = article.dropdown();

  dropdown_item.appendChild(title);
  dropdown_item.appendChild(body);

  dropdown_item.addEventListener('click', function(event){

    let modal_title = document.getElementById("modal-title")
    let modal_body = document.getElementById("modal-body")
    let modal_img = document.getElementById("modal-img")

    modal_title.textContent = article.title;
    modal_body.textContent = article.body;
    modal_img.src = article.image_url;

  });

  dropdown_item.setAttribute("data-toggle", "modal")
  dropdown_item.setAttribute("data-target", "#article-modal")

  return dropdown_item;
}


function showMatchesInDropdown(matching_articles){

  let max_results = 3;

  let end = Math.min(matching_articles.length, max_results);

  let dropdown = document.getElementById("search-results")

  dropdown.replaceChildren();

  for(let index = 0; index < end; index++){

    let article = matching_articles[index];

    let dropdown_item = createDropdownItem(article);

    dropdown.appendChild(dropdown_item);
  }

  let search_input = document.getElementById("searchbar");
    search_input.addEventListener("focusout", function(event){

      window.setTimeout(this.replaceChildren.bind(dropdown), 1000);
  })
}


function setUpSearchBar(articles){

  let input = document.querySelector('input');

  input.addEventListener('change', function(event){

    if(event.target.value !== ''){
      let matching_articles = filterArticles(articles, event.target.value);

      showMatchesInDropdown(matching_articles);
    }
  });
}


function setupPagination(cards_per_page, articles){


  let page_links = Array.from(document.getElementsByClassName("page-link"));

  for(let link of page_links){

    if(link.textContent === "Previous") {

      link.addEventListener("click", function(event) {
        if(current_page !== 1) {
          current_page -= 1
          addArticlesToCards(cards_per_page, articles, current_page);
        }
      });
    }
    else if(link.textContent === "Next") {

      link.addEventListener("click", function(event) {
        if(current_page !== 3) {
          current_page += 1
          addArticlesToCards(cards_per_page, articles, current_page);
        }
      });
    }
    else{

      link.addEventListener("click", function(event) {
        current_page = parseInt(link.textContent);
        addArticlesToCards(cards_per_page, articles, current_page);
      });
    }
  }
}
